import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "../store";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: function () {
      if (store.getters.is_authenticated) {
        return import ('../views/Home.vue')
      } else {
        return import ('../views/Auth.vue')
      }
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: function () {
      if (!store.getters.is_authenticated) {
        return import ('../views/Auth.vue')
      } else {
        return import ('../views/Home.vue')
      }
    }
  },
  {
    path: '/logout',
    name: 'Logout',
    component: function () {
      return import ('../views/Auth.vue')
    }
  },
  {
    path: '/*',
    name: 'Other',
    component: function () {
      this.path = "/"
      return import ('../views/Auth.vue')
    },

  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
