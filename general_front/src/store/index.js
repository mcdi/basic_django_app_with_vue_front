import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";
import router from "../router";

Vue.use(Vuex)
Vue.use(axios)

const store = new Vuex.Store({
    state: {
        auth: {
            access_token: localStorage.access_token
        }
    },
    getters: {
        is_authenticated: state => {
            if (state.auth.access_token !== null && state.auth.access_token !== "null") {
                return true
            } else {
                return false
            }
        }
    },
    mutations: {
        setToken: function (state, token) {
            localStorage.access_token = token
            state.auth.access_token = token
        },
    },
    actions: {
        login(context, payload) {
            axios
                .post(
                    "/api/jwt-auth/token/",
                    {
                        username: payload.username,
                        password: payload.password
                    }
                )
                .then(response => (
                    context.commit('setToken', response.data.access),
                    router.push("/")
                ))
                .catch(error => {
                    alert("Ошибка авторизации: " + error)
                });
        },
        logout(context){
            context.commit('setToken', null)
        }
    },
    modules: {

    }
});

export default store;