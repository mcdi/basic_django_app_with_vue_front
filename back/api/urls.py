from django.urls import include, path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='API')
urlpatterns = [
    path('', schema_view)
]

urlpatterns += [
    path('jwt-auth/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('jwt-auth/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]